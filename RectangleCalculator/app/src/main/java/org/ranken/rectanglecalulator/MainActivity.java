package org.ranken.rectanglecalulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import 	java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {

    Button buttonCalc;
    TextView textViewAreaAmt;
    TextView textViewPerimeterAmt;
    EditText editTextWidth;
    EditText editTextHeight;
    float mValueOne;
    float mValueTwo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buttonCalc =(Button) findViewById(R.id.buttonCalc);
        textViewAreaAmt = (TextView) findViewById(R.id.textViewAreaAmt);
        textViewPerimeterAmt = (TextView) findViewById(R.id.textViewPerimeterAmt);
        editTextHeight = (EditText) findViewById(R.id.editTextHeight);
        editTextWidth = (EditText) findViewById(R.id.editTextWidth);

        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DecimalFormat dtime = new DecimalFormat(".##");
                DecimalFormat dtime = new DecimalFormat("#.00");

                mValueOne = Float.parseFloat(editTextWidth.getText().toString() + "");
                mValueTwo = Float.parseFloat(editTextHeight.getText().toString() + "");

               textViewAreaAmt.setText(dtime.format(mValueOne * mValueTwo)  + "");
               textViewPerimeterAmt.setText(dtime.format(mValueOne *2 + mValueTwo * 2) + "");

                String str = editTextWidth.getText().toString() + " * " +
                        editTextHeight.getText().toString() + " = " +
                        textViewAreaAmt.getText().toString();

                Toast t = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
                t.show();

                Log.e ("Width", editTextWidth.getText().toString());
                Log.e ("Height", editTextHeight.getText().toString());
                Log.e("Area", textViewAreaAmt.getText().toString());


            }

        });
    }
}
